This is a house with 3 bedrooms, 1 bathroom and 1 toilet.

The building has an irregular shape and is approximately 80 square meters in size.  There are 5 single-use rooms and no open plan areas in this floorplan. The main entrance is located at the bottom centre of the floorplan. 

Starting from the main entrance, the rooms in clockwise order are open plan entry, open plan dining, open plan living, open plan kitchen, bathroom, toilet, bedroom 3, bedroom 2 and bedroom 1. 1 corridor is situated internally. 

The open plan entry is connected to outside. The bathroom is connected to open plan corridor. The toilet is connected to open plan corridor. The bedroom 3 is connected to open plan corridor. The bedroom 2 has is connected to open plan corridor. The bedroom 1 is connected to open plan corridor.  